README
=======

#Authoren:
-----------
Christian Bay (christian.bay@fau.de)
Stephan Gabert (stephan.gabert@fau.de)

#Inhalt:
-----------
Bei diesem Skript handelt es sich inhaltlich um eine Zusammenfassung
der Lehrveranstaltungen "Biologie für Nebenfächler" und dem Praktikum
"Morphologie und Anatomie der Organismen"
an der Friedrich-Alexander-Universität Erlangen-Nürnberg.

#Mitwirken:
-----------
Ergänzungen, Verbesserungen etc. sind sehr erwünscht.
Fühlt euch frei das Skript zu erweitern.
Schickt einfach einen Patch an eine der obigen Email Adressen
oder forkt es auf GitHub und sendet dann einen Pull Request.

#Installation:
------------
Zum übersetzen wird Tex benötigt. Beim Makefile wird zudem das Paket
'latexmk' vorrausgesetzt.

Bauen des PDF Skripts:

    make

Kontinuierliches bauen bei Änderungen von Source Dateien:

    make continuous

# Aktueller Stand
--------
Die neuste Version des PDF findet man [hier](https://fsi.informatik.uni-erlangen.de/dw/_media/pruefungen/nebenfach/bio_skript_2014.pdf).

#Kontakt:
----------
Bei Fragen, Anregungen etc. einfach an oben genannte Email Adressen wenden.