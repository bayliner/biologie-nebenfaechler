% vim: set et ts=4 sw=4:

\section{Botanik}
\subsection{Komponenten einer Zelle}

\underline{\colorbox{YellowGreen}{Klausurrelevant: Inhalte einer Zelle}}
\begin{figure}[h]
    \begin{center}
        \includegraphics[scale=0.7]{pics/botanik/zelle.png}
        \caption{Pflanzenzelle}
    \end{center}
\end{figure}
\begin{itemize}
    \item Mitochondrion: Energiegewinnung durch Zellatmung (z.B. Citratzyklus, Oxidation von Glucose
        im Cytoplasma)
    \item Chloroplasten: Herstellung von energiereicher Glucose/St\"arke durch Photosynthese
    \item Zellkern: enthält die DNA
    \item {Golgi-Apparat (Membranumschlossener Reaktionsraum innerhalb der Zelle):
            \begin{itemize}
                \item Bildung und Speicherung sekretorischer Vesikel (auf zellulärer Ebene
                    \textit{ausgeschlossene} Proteine), z.B. extrazelluläre Matrix, Transmitter/Hormone
                \item Synthese und Modifizierung von Elementen der Plasmamembran
                \item Bildung von lysosomalen Proteinen (primäres Lysosom)
        \end{itemize} }
    \item {Peroxisome (veraltet \textit{Microbodies} ): mit Zellmembran umschlossene Entgifungsapparate
        (= abgetrennte Reaktionsräume für schädliche Reaktionen). U.a. Sauerstoffabbau.}
    \item Vakuole \begin{itemize}
            \item Stoffspeicher (z.B. Proteine)
            \item feste Zellwand (stabilisierendes Innenskelett)
            \item Aufblähen der Zelle (durch osmotische Aufnahme v. Wasser)
            \item u.v.m.
        \end{itemize}
\end{itemize}

\textbox{Wo befinden sich in der pflanzlichen Zelle welche Genome?}{
    Der \textbf{Zellkern} enthält die eigentliche DNA.\\
    \textbf{Chloroplasten} und \textbf{Mitochondrium} sind \underline{Endosymbionten}: Sie haben sich aus
    eigenständigen Prokaryonten entwickelt und wurden in die Zelle aufgenommen
    (ringförmige eigene DNA und 70S Ribosomen\footnote{
        Der Sedimentationskoeffizient ist der Quotient aus der maximalen Sedimentationsgeschwindigkeit
        eines Teilchens in einer Zentrifuge geteilt durch die Stärke des Zentrifugalfelds. Maßeinheit: $10^{-13}s$
    }
    sprechen für Endosymbiontentheorie).
}
~\\
\textbox{Alleinstellungsmerkmale für Pflanzen}{
    \begin{itemize}
        \item Vakuole (Membran der Vakuole: \textbf{Tonoplast})
        \item Zellwände
        \item Chloroplasten
    \end{itemize}
}
~\\
\textbox{Zellwand-Aufbau der Pflanzenzelle (au{\ss}en $\rightarrow$ innen)}{
    \begin{enumerate}
        \item Mittellamelle
        \item Primärwand
        \item Sekundärwand
        \item (Tertiärwand)
        \item Membran (nicht mehr Teil der Zellwand)
    \end{enumerate}
}

\subsection{Bestandteile einer pflanzl. Zelle}
\subsubsection{Atome}
\begin{itemize}
    \item Kohlenstoff (jede organische Verbindung enthält Kohlenstoffatome)
    \item Sauerstoff
    \item Wasserstoff
    \item Schwefel (als Sulfate; einige Aminosäuren z.B Cystin)
    \item Phosphor (als Phosphate; z.B. \textbf{A}denin\textbf{T}ri\textbf{P}hosphat)
    \item Kalium, Calcium (Pektinstoffe der Zellwände), Magnesium (Chlorophyll), Eisen
    \item Sonderfall: Strontium, Cobalt
    \item Spurenelemente: Kupfer, Zink, Bor, Mangan
    \item Natrium wird \textbf{nicht} benötigt {\tt :-)}
\end{itemize}

\subsubsection{Moleküle}
\label{sec:molekuele}
\begin{itemize}
    \item Wasser
    \item Zucker (DNA, RNA, Ribosomen, für Glykolisierung (Entgiftung))
    \item Fette (Zellmembran $=$ Lipidschicht)
    \item Fettsäuren (Glycerin + langkettige Carbonsäuren)
    \item DNA, RNA
    \item Proteine
    \item Aminosäuren (Bausteine von Proteinen)
\end{itemize}

\paragraph{Zucker}
\textbox{Nennen Sie 2 glykosidische Bindungen}{
    \textbf{Cellulose: $\beta$-1,4 glykosidische Bindung\\
    Amylose: $\alpha$-1,4 glykosidische Bindung}
}
\vspace*{10pt}

\begin{figure}[h]
    \begin{center}
        \includegraphics[scale=0.8]{./pics/botanik/saccharose_glykosidische_bindung.jpg}
        \caption{Bildung der Saccharose (Sucrose)}
    \end{center}
\end{figure}

\vspace{10pt}

Mehrfachzucker (Polysaccharide) sind Kohlenhydrate, in welchen (mind. 10) Einfachzucker (Monosaccharide)
über glycosidische Bindungen miteinander verbunden sind.\\
Glucose: $C_6H_{12}O_6$ mit reduzierender Aldehyd-Gruppe.\\
Fructose: $C_6H_{12}O_6$ mit Keto-Gruppe.\\
Weitere Zucker: Maltose, Amylose, Cellulose, etc.\\
\colorbox{YellowGreen}{Nicht Klausurrelevant}: Zuckerformeln und chemische Strukturen

\paragraph{Fettsäuren und Fette}{
    \begin{figure}[h]
        \begin{center}
            \includegraphics[width=\textwidth]{pics/botanik/phospholipiddoppelschicht.jpg}
            \caption{Phospholipiddoppelschicht}
        \end{center}
    \end{figure}

    Die Moleküle der Membran des Liposoms bestehen aus
    aus einem \textbf{hydrophoben (lipophilen) ungeladenen
    Rumpf (Fettsäuren)} und aus einem
    \textbf{hydrophilen (lipophoben) geladenen Kopf (Glycerin)}.\\

    Gesättigte Fettsäuren: Alle C-Atome sind vollkommen mit
    H-Atomen gesättigt ($sp_3$-hybridisiert).\\

    Wenn sich mehrere Fette zusammenfügen, bilden sie oftmals
    einen Kreis oder eine Bilipidmembran
    (au{\ss}enliegender hydrophiler Teil).\\

    Wasserlösliche Makromoleküle können dadurch an die Membran
    angelagert werden, wie z.B. Proteine, die sich aus dem
    Zellinneren in die Membran integrieren.\\

    Der Mensch kann nur gesättigte Fettsäuren herstellen.
    Ungesättigte Fettsäuren sind zum überleben essentiell
    und müssen durch Nahrung zugeführt werden.

    Besonders gesund z.B. die Omega-3-Fettsäuren. \footnote{
        Öle mit höchstem ungesättigten Fettsäuregehalt:
    Fischöl, Leinöl, Lebertran}
}

\paragraph{tRNA, mRNA, rRNA und DNA} {
    \begin{itemize}
        \item \textbf{DNA} (\textit{Desoxyribonucleinsäure}): Desoxyribose (oxidiert am 2. C Atom) + Phosphat.
        \item In DNA liegen Aminosäuren paarweise aneinander: (Adenin-Thymin, Guanin-Cytosin).
        \item \textbf{Purin}: Adenin, Guanin.\\ \textbf{Pyrimidin}: Thymin, Cytosin.
        \item { DNA ist in Form einer \textbf{Doppelhelix}
                \footnote{
                    Die fünf Kohlenstoffatome einer Desoxyribose sind von 1'
                    (sprich Eins Strich) bis 5' nummeriert. Am 1'-Ende dieses Zuckers
                    ist die Base gebunden. Am 5'-Ende hängt der Phosphatrest.
                }
                aufgebaut.
                Einer der zwei Str"ange enthält die genetische Information
                und der andere Strang enth"alt jeweils die komplementären Aminosäuren
                (\glqq sense\grqq- + \glqq antisense\grqq-Strang).
            }
        \item RNA (\textit{Ribonucleinsäure}): Ribose + Phosphat.
    \end{itemize}

    \begin{figure}[h]
        \begin{center}
            \includegraphics[width=\textwidth]{pics/botanik/dna_aufbau.jpg}
            \caption{Aufbau der DNA}
        \end{center}
    \end{figure}


    \textbox{tRNA}{Die transfer-RNA kodiert keine genetische
        Information, sondern dient als Hilfsmolekül bei der
        Proteinbiosynthese, indem sie eine einzelne Aminosäure aus
        dem Cytoplasma aufnimmt und zum Ribosom transportiert.
        Die tRNA wird durch ein bestimmtes RNA-Gen kodiert.
    }
    ~\\

    \textbox{mRNA}{Die messenger-RNA ist das
        RNA-Transkript\footnote{
            Transkription: Synthese von RNA anhand der DNA-Vorlage
        }
        eines zu einem Gen gehörigen Teilabschnitts der DNA.

        Die mRNA wird bei der \textit{Transkription} von der
        RNA-Polymerase erzeugt.
        Bei der Translation wird diese mRNA nun an die
        Ribosomen\footnote{
            Bei Eukaryonten im Zellkern, Bei Prokaryonten
            im Cytoplasma
        }
        binden, an welcher das zur mRNA entsprechende Protein
        synthetisiert wird.
        Es binden sich i.d.R. mehrere Ribosome gleichzeitig an eine
        mRNA.
        Nach der Synthese des Proteins löst sich die mRNA wieder und
        kann von der RNAse in ihre Einzelteile zerlegt werden.
    }
    ~\\
    \textbox{rRNA}{Die ribosomale RNA bildet (zusammen mit
        ca. 50 (Prokaryonten) bzw. 80 ribosomalen Proteinen) die
        Ribosomen.
        In diesem Verbund synthetisiert die rRNA die von der
        Zelle benötigten Proteine.
    }
}

\paragraph{Erzeugen eines Proteins}{
    \textbf{DNA} $\rightarrow$ (Transkription) $\rightarrow$ \textbf{pre-mRNA}$\rightarrow$ (Processing, z.B.
    Splicing\footnote{
        Eliminieren der Introns(Pseudo-Nonsense-Sequenzen) aus der RNA.
        Freie Enden der Exons werden miteinander verbunden.
    }
    )\\
    $\rightarrow$ \textbf{mRNA} $\rightarrow$ (Translation)  $\rightarrow$ \textbf{Protein}
}


\paragraph{Transkription} {
    \begin{floatingfigure}[l]{0.6\textwidth}
        \centering
        \includegraphics[width=0.5\textwidth]{pics/botanik/transkription.png}
        \caption{Transkription}
    \end{floatingfigure}

    Bei der Transkription wird ein Gen abgelesen und als RNA-Molekül vervielfältigt,
    das heißt ein spezifischer DNA-Abschnitt dient als Vorlage zur Synthese eines neuen RNA-Strangs.
    Dabei wird die mRNA - anhand der DNA - durch die entsprechende RNA-Polymerase
    \colorbox{YellowGreen}{(von 3' nach 5')} synthetisiert.
    Hierbei wird Uracil anstatt Thymin eingebaut.
}
\begin{figure}[H]
    \begin{center}
        \includegraphics[scale=0.5]{pics/botanik/dna_exons_introns.png}
        \caption{DNA Exons Introns}
    \end{center}
\end{figure}

\paragraph{Translation}{
    In der Translation wird das entsprechende Protein aus seiner mRNA synthetisiert.
    Dies läuft in Ribosomen\footnote{
        Ribosomen: Organelle der Translation, bestehend aus 60$\%$ rRNA und 40$\%$ Protein
    }
    ab.

    Hierbei binden sich mehrere Ribosome an die mRNA
    und synthetisieren von \colorbox{YellowGreen}{5' nach 3'} die Proteine.

    tRNAs fungieren hierbei als Adapter zwischen Aminosäuren und  mRNA-Abschnitten.
    Dabei bindet die tRNA mit ihrem Anticodon an das entsprechende Codon\footnote{
        Ein Codon ist ein Basentriplett.
        Startcodon: AUG.  Endcodon: UAA, UAG, UGA
    }
    des mRNA-Abschnitts.

    \begin{figure}[h]
        \begin{center}
            \includegraphics[scale=0.5]{pics/botanik/translation.png}
            \caption{Translation}
        \end{center}
    \end{figure}

    Danach verbindet sich die jeweilige Aminosäure mit dem bisherigen Protein und
    die tRNA löst sich wieder.

    \vspace{10pt}

    \textbox{Strukturen der Aminos"aure-Ketten}{
        \begin{itemize}
            \item Primärstruktur: Aminosäure-Kette
            \item Sekundärstruktur: Unterstrukturen wie Schrauben ($\alpha$-Helix) und
                Faltungen ($\beta$-Faltblatt), durch Wechselwirkung der Aminosäuren.
            \item Tertiärstruktur: Gesamtform des Proteins
            \item Quartärstruktur: Mehrere Proteine zu Superkomplexen zusammenlegen
        \end{itemize}
    }
    \begin{figure}[H]
        \begin{center}
            \includegraphics[width=\textwidth]{pics/botanik/tRNA_strukturen.jpg}
            \caption{Struktur der tRNA}
        \end{center}
    \end{figure}

}

\begin{center}
    \underline{Mitschrift vom 28.10.2013}
\end{center}
\subsection{Algen}
\label{sec:Algen}
Algen gehören streng genommen nicht direkt zu den Pflanzen.

Es bildeten sich erst Blaualgen (\textit{heute} Cyanobakterien genannt), die
Sauerstoff produzierten, noch bevor es Mitochondrien gab.\\

Eigenschaften von Algen:
\begin{itemize}
    \item Sonderfall Blaualgen: gram-negative Bakterien (Prokaryonten)
    \item Stickstofffixierung
    \item Binden $CO_2$ 
    \item Teil der Nahrungsketten
    \item Selbstreinigung von Gewässer (Stickstoff-, Phosphorverbindung binden;
        Schwermetalle in Thallus anlagern)
    \item Symbionten für Pilze
\end{itemize}

Das Algenwachstum ist v.a. an Küsten und Polen (Eisengehalt durch Eisschmelze) erh"oht.
Bei zu vielen Algen droht Eutrophie $\rightarrow$ Sauerstoffarmut in Meeren 
wegen Zersetzung der abgestorbenen Algen durch Bakterien/Pilzen und aufgrund n"achtlicher Zellatmung.

\subsection{Bauplan einer Blütenpflanze/Kormus}
Bl"utenpflanzen (auch \textit{Bedecktsamer}) werden in Monokotyledonen und (Eu)Dikotyledonen
aufgeteilt, wobei diese sich in der Anzahl ihrer Keimbl"atter unterscheiden:

Monokotyledonen sind \textbf{Einkeimbl"attrige} und Dikotyledonen sind \textbf{Zweikeimbl"attrige}.\\

Einige Vertreter der Monokotyledonen:
\begin{itemize}
    \item Lilienartige
    \item Spargelartige (u.a. Orchideen)
    \item Palmenartige
\end{itemize}

Eine Bl"utenpflanze ist wie folgt aufgebaut:
\begin{itemize}
    \item \textbf{Bl"atter}, entstehen an den Knoten (Nodi)
    \item{
            \textbf{Spross} unterteilt in Nodium (Sprossachse) und Internodium.\\
            Sonderrolle \textbf{Blüte}: unverzweigter Kurzspross mit begrenztem Wachstum,
            dessen Blätter (in)direkt im Dienst der geschlechtlichen Fortpflanzung stehen.
        }
    \item \textbf{Wurzel}
\end{itemize}

Pflanzen wachsen ihr Leben lang, wobei Spross und Wurzel an ihren Enden (Spitzen) wachsen.\\
Dort befinden sich sogenannte Meristeme: Omnipotente \glqq Stammzellen\grqq, welche ununterbrochen neue Zellen
bilden und diese nach unten abgeben.\\
Sonderrolle: Bäume haben im ganzen Pflanzenkörper laterale Meristeme \footnote{
    Neben den sekundären Meristemen in den Achseln der Laubbl"atter und der Wurzelspitzen,
auch das Kambium und das Phellogen ($\rightarrow$ sek. Dickenwachstum)}.\\

In den Achselknospen (zwischen Blattstiel und Spross) befinden sich
axilläre Meristeme (Bildung von Seitentrieben).\\
Bei mehrjährigen Pflanzen treten oft laterale Meristeme auf, welche zum sekundären Dickenwachstum
führen oder neues Abschlussgewebe bilden.\\
Interkalare Meristeme befinden sich vor allen in Sprossachsen,
z.B. an den Knoten von Gräsern (Unempflindlichkeit gegen Rasenschnitt).\\
\textbf{Apex}: Vegetationskegel an der Spitze des Sprosses, an welchem sich das Längenwachstum vollzieht.

\subsubsection{Blatt}
\label{sec:blatt}
Arten:
\begin{itemize}
    \item { Keimblätter:
            Sie sind die ersten Blätter des Keimlings.
        Aufgabe ist meistens die Pflanze zu ern"ahren, bis die Photosynthese m"oglich ist.}
    \item Laubblätter: Dienen der Photosynthese
    \item Hochblätter: Anlockung von Insekten zur Bestäubung (z.B. durch Farbe), schauen nur wie Blütenblätter aus.
    \item Blütenblätter: Bl"atter die zur Fortpflanzung und Anlockung dienen (Sex).
\end{itemize}


\begin{minipage}[t]{0.4\textwidth}
    \begin{enumerate}[label={\alph*)}, leftmargin=0cm]
        \item \textbf{Obere Epidermis} mit Cuticula (Wachsschicht):\\
            Schutzschicht ($\neq$ Zellschicht) ohne Chloroplasten.
        \item \textbf{Palisadenparenchym}:\\
            Hauptort der Photosynthese. Chloroplasten und Vakuole.
        \item \textbf{Schwammparenchym}:\\
            Interner Zellraum zum Gasaustausch bei der Photosynthese.
        \item \textbf{Untere Epidermis} mit Spaltöffnung (zur Wasserabsonderung)\\
    \end{enumerate}
\end{minipage}
\begin{minipage}[t]{0.6\textwidth}
    \vspace{0pt}

    \begin{center}
        \includegraphics[width=\textwidth]{pics/botanik/blatt_aufbau1.jpg}
        \captionof{figure}{Aufbau des bifazialen Laubblattes}
    \end{center}
\end{minipage}


\textbf{Einschub zum Gasaustausch}:\\
Durch Osmose füllt sich die Vakuole mit Teilchen.  Dies übt Druck auf die Spaltöffnung aus, sodass
diese sich zum Gasaustausch öffnet.\\


Blättertypen:
\begin{itemize}
    \item \textbf{Lichtblatt}: Ausgeprägte Cuticula. Kann viel Licht aufnehmen und $CO_2$ binden.
        Energieaufwändger, weil komplexerer Zellaufbau. Zwei bis mehrschichtiges Palisadenparenchym.
        Andere Chloroplastenmorphologie
    \item \textbf{Schattenblatt}: komplementär zu Lichtblaettern.
    \item \textbf{Xeromorphe Bl"atter}: Versenkte Spaltöffnung. Dicke Epidermis.
    \item \textbf{Hygromorphe Bl"atter}: Erhobene Spalt"offnung zur besseren Transpiration. H"aufig fehlende
        Cuticula. Einschichtige Epirdermis.
\end{itemize}


\vspace*{10pt}
\textbox{Nennen Sie 3 der 4 Metamorphosen des Blatts}{
    \begin{itemize}
        \item Blatt(fieder)ranken
        \item Phyllodien (Rückbildung zu Sprossähnlichem Gebilde)
        \item (Neben)Blattdornen.\\
            Ausprägungen: Fangblätter (Sonnentau, Venusfliegenfalle), Kakteenstachel
    \end{itemize}
}

\begin{center}
    \underline{Mitschrift vom 04.11.2013}
\end{center}

\subsubsection{Spross}

\begin{figure}[h]
    \centering
    \includegraphics[width=\textwidth]{pics/botanik/blatt_leitbuendel.jpg}
    \caption[Leitbündel im Spross der Dikotyledonen]
    {Aufbau eines Leitbündel (im Spross der Dikotyledonen \textit{ringförmig angeordnet}).
        \textbf{Phloem} zeigt in Richtung Epidermis. \textbf{Xylem} zeigt in Richtung Markh\"ole.
        \textit{\underline{Zeichnen von Blattaufbau und Leitbündelstruktur ist \textbf{klausurrelevant}}}
    }
\end{figure}


\begin{itemize}
    \item {\textbf{Xylem} (innen) \\
            Das Xylem transportiert Wasser und lösliche, mineralische Nährstoffe von
            den Wurzeln durch und in die Pflanze.\\
            \textbf{Tracheen} bilden die eigentlichen Röhren, in welchen das Wasser
            transportiert wird. Diese Interzellulargänge sind abgestorbene Zellen
            (Stichwort: \textit{programmierter Zelltod}).
        }
    \item {\textbf{Phloem} (außen) \\
            Das Phloem transportiert Nährstoffe durch die Pflanze.
            Die Siebröhrenglieder (i.d. der Transport stattfindet) bilden mit den
            Geleitzellen (für Stoffwechsel zuständig) eine funktionelle Einheit.
            Die \textit{Geleitzellen} beladen die \textit{Siebröhren} mit Zucker
            aus den Blättern.
        }

    \item Xylem und Phloem liegen sich direkt gegenüber!

    \item {\textbf{Kollenchym} \\
            Noch wachstums- und dehnungsfähiges, nicht verholztes Festigungsgewebe
            aus lebenden Zellen. Verdickt die \textbf{Primärwand}.
            Stabilisierung und Biegsamkeit von Spross.
            Älteres Kollenchym-Gewebe kann absterben und sich Sklerenchymzellen entwickeln.
        }

    \item {\textbf{Sklerenchym} \\
            Tritt meist als Schicht um ein Leitbündel auf (Spargel).
            Verdickt die Sekundärwand (= verholzte Zellwand).
            Durch Einlagerung von Lignin in die Sekundärwand sterben die Zellen ab.
        }
\end{itemize}


\label{unterschied_monokot_dikot}
\begin{minipage}[t]{0.5\textwidth}
    \begin{center}
        \textbf{Monokotyledonen}
    \end{center}
    \begin{itemize}
        \item {Kein Kambium im Leitbündel\\
                $\rightarrow$ Nicht zum sekundären Dickenwachstum (Holzbildung) fähig.
            }
        \item {\textbf{Leitbündelscheide} (LBS) stabilisiert das Leitbündel.
                \textit{\glqq Stahl im Stahlbeton\grqq}
            }
        \item Xylem + Phloem + LBS $=$ geschlossenes kollaterales Leitbündel. \\
    \end{itemize}
\end{minipage}
\begin{minipage}[t]{0.5\textwidth}
    \begin{center}
        \textbf{Dikotyledonen}
    \end{center}
    \begin{itemize}
        \item Phloem und Xylem sind durch Kambium getrennt.
        \item {\textbf{Kambium} ist für sekundäres Dickenwachstum (Holzbildung)
                verantwortlich. Kambium liegt direkt zwischen Xylem und Phloem.
            }
        \item offenes kollaterales Leitbündel
    \end{itemize}
\end{minipage}

\begin{minipage}[t]{0.5\textwidth}
    \begin{itemize}
        \item 1 Keimblatt
        \item Parallelnervig
        \item mehrfaches von 3 Blütenblätter (Bsp. Lilie)
        \item verstreute Leitbündel
        \item (meist) kein sekundäres Dickenwachstum (aufgrund vom Fehlen des Kambiums)
    \end{itemize}
\end{minipage}
\begin{minipage}[t]{0.5\textwidth}
    \begin{itemize}
        \item 2 Keimblätter
        \item Blattadern sind netzförmig angeordnet
        \item 4 oder 5 Blütenblätter (Bsp. Rose)
        \item ringförmig angeordnete Leitbündel
    \end{itemize}
\end{minipage}

\paragraph{Meristeme}
%Wie Meristeme Pflanzenkörper erzeugen: Nicht Klausurrelevant!
Drei Grundgewebe im Pflanzenkörper:
\begin{itemize}
    \item Abschlussgewebe (Epidermis)
    \item {Grundgewebe (Kollenchyme, Parenchyme usw.)\\
            Kollenchym: Dicke Primärwand mit dünnen Stellen für
        Tausch mit anderen Zellen)}
    \item {Leitgewebe (Xylem, Phloem)} % Tracheen

\end{itemize}

\paragraph {Sekundäres Dickenwachstum:}
Wird verursacht durch Kambium, welches einen konzentrischen Ring bildet und
anf"angt Zellen noch innen (Holz) oder nach au"sen (Bast) abzugeben.

Es bildet sich ab einer gewissen Grö"se ein zweiter konzentrischer Ring.
Das \textit{Korkkambium} bildet die Borke des Baums.

In Nadelbäumen sind bis zu 40\% des Holzes \glqq lebendig\grqq, während beim
Laubbaum nur bis zu 14\% \glqq lebendig\grqq sind.

\paragraph{Holz}
Das Holz der Nadelbäume ist deutlicher einfacher aufgebaut als das
Holz der Laubbäume und besteht aus drei Zelltypen.

Hauptbestandteil sind die Tracheiden (dünnwandige Leitgefäße), die der Wasserleitung dienen.
Neben ihnen sind noch Zellen des Markstrahl- und Holzparenchyms vorhanden.

Bei vielen Nadelbäumen ist das Holz von Harzkanälen (von Holzparenchym umgeben) durchzogen, aus denen bei
Verletzungen des Stammes das Harz austritt.

Die Markstrahlen verlaufen vom Bast in Richtung Mark, bestehen aus lebenden Parenchymzellen,
versorgen den Holzkörper mit den im Bast produzierten Stoffwechselprodukten und dienen auch als Speicherort.

Bei allen Bäumen erlischt die Wasserleitfähigkeit des Leitgewebes mit der Zeit
und auch die lebenden Zellen sterben ab.
\footnote{Quelle: http://www.uni-duesseldorf.de/MathNat/Biologie/Didaktik/Holz/dateien/gymno.html}

\paragraph{Sekundäres Abschlussgewebe}
Epidermis und Rinde ("au"serstes Abschlussgewebe) k"onnen dem sekund"aren Dickenwachstum zumeist kaum folgen.
Daher bildet das Phellogen (Korkkambium) nach au"sen Kork und nach innen eine d"unne Schichte Korkrinde
(Phelloderm) aus.

Diese Rindenschicht bietet zwar einen sehr guten Schutz,
doch da sie Wasser- und Luftdicht ist, w"urde der Baum nicht mehr genug mit $O_2$ versorgt werden.

Daf"ur gibt es eine Art Lüftungsgewebe - die\textbf{Lentizellen}:

Bereiche der Borke, in denen eine durch Aufreißen der obersten Zellschicht entstandene Öffnung
mit darunterliegenden abgestorbenen Korkzellen einen Gasaustausch zwischen der Umgebungsluft
und dem lebenden Gewebe (unterhalb der Lentizelle) ermöglicht.\\

\textbox{Nennen Sie 3 Metamorphosen der Sprossachsen}{
    \textbf{Wasserspeicher} (Kakteen), \textbf{Rhizom} (sieht aus wie Wurzel, z.B. Ingwer),
    \textbf{Sprossknolle} (z.b. Kartoffel), \textit{Sprossdorn}, \textit{Sprossranke},
}

\subsubsection{Wurzel}
\label{sec:wurzel}
Monokotyle Pflanzen (homorhizer Wurzeltyp):\\
Die Keimwurzel (\textbf{prim"are Wurzel}) verkümmert sehr schnell.
Es bilden sich seitliche sog. sprossbürtige Wurzeln (keine Hauptwurzel mehr erkennbar).\\
\vspace{5pt}

Dikotyledonen (allorhizer Wurzeltyp):\\
Hauptwurzel wächst meist positiv geotrop (senkr. i. d. Boden) und bildet ständig Seitenwurzeln aus.

\paragraph{Unterschied zw. Spross und Wurzel}
\textbox{Unterschiede zu Spross:}{
    Wurzeln haben
    \begin{itemize}
        \item Keine Blätter
        \item Radiales Leitsystem (innere Zentralzylinder)
        \item Wurzelhaube
    \end{itemize}
}

\paragraph{Aufgaben}
\label{sec:wurzel_aufgabe}
\textbox{Was sind die Aufgaben der Wurzel?}{
    \begin{itemize}
        \item \textbf{Stoffspeicherung}
        \item \textbf{Wasser- und Nährstoffaufnahme}
        \item \textbf{Festhalten von Pflanze im Boden}
        \item \textbf{Symbiose mit Pilzen und Bodenbakterien} (Stickstofffixierung)
        \item Synthese von Phytohormonen (pflanzeneigene wachstumregulierende Hormone)
        \item usw.
    \end{itemize}
}

Zudem befinden sich in der Wurzelspitze (der \textbf{Wurzelhaube}) spezialisierte Zellen, in denen
aufgrund der Fallrichtung schwerer Partikel die Wuchsrichtung gen Erdmittelpunkt bestimmt werden kann.

\paragraph{Aufbau der Wurzel}
\label{sec:wurzel_aufbau}
\textbf{Au"sen $\rightarrow$ Innen:}
\begin{itemize}
    \item Rhizodermis (einschichtiges Abschlussgewebe)
    \item Exodermis (ein- bis mehrschichtiges Abschlussgewebe)
    \item Wurzelrinde (i.d.R. ein farbloses Parenchym)
        %\item Durchlasszelle (nur in manchen Zellen)
    \item Endodermis (innerste Schicht der Rinde. Kontrolliert Wasser- und N"ahrsalzdurchtritt)
    \item Perizykel (äußerster Teil des Zentralzylinders, der die Seitenwurzeln ausbildet)
    \item Leitb"undel (Phloem u. Xylem)
\end{itemize}

\paragraph{Wasseraufnahme}
"Uber die \textbf{Wurzelhaare} und feine Seitenwurzeln erfolgt die Wasseraufnahme (via negativem Wasserpotential).

Der \textbf{\textit{apoplasmatische} Bereich} (außerhalb der Zelle) saugt bei Feuchtigkeit Wasser an.

Das Wasser gelangt vom Boden über das Rindenparenchym bis an die Endodermis.
Dies kann entweder im Zellinneren (\textit{symplastisch}), oder im
Zellwandbereich (\textit{apoplasmatisch}) erfolgen.

Durch den \textbf{Casparischen Streifen} muss das Wasser die Endodermiszellen passieren
und der apoplastische Transport wird blockiert.

Dadurch wird auch die ungewollte Aufnahme von Giften in den Zentralzylinder
(zentraler Teil der Sprossachse) an der Endodermis kontrolliert.

\paragraph{Theorie des Wassertransports}
Der Transport des Wassers von der Wurzel zu den Blättern benötigt keine Energie.
Es bildet sich ein Wasserfaden zwischen Wurzel und Blättern. Wenn in diesem Faden
Wasser verdunstet, wird dieses nachgesaugt (Adhäsionskräfte).

\paragraph{Metamorphosen}
\begin{itemize}
    \item Speicherwurzel (z.B. Orchideen)
    \item Atemwurzel (z.B. Mangroven)
    \item Verdickung vom Wurzel und/oder Hypokotyl (Bereich zwischen Spross und Wurzel)
    \item Konkurrenz und Luftwurzel: Würgefeige
\end{itemize}

\subsection{Fortpflanzung bei Pflanzen}
\subsubsection{Mitose}
Die \textit{Mitose} ist der Vorgang der Zellkernteilung eukaryotischer Zellen.
Anschließend werden in der sog. \textbf{Cytokinese} (Zellteilung) das Zellplasma und die Bestandteile
der Mutterzelle auf die Tochterzellen aufgeteilt.\\

Bei der Mitose werden Schwester-Chromatiden voneinander
getrennt und den zwei Tocherzellkernen zugeführt, die dann Chromatiden
(nicht replizierte Tocherchromosomen) mit \underline{gleichem} Genbestand
enthalten.
\begin{figure}[h]
    \begin{center}
        \includegraphics[width=\textwidth]{pics/botanik/mitose.jpg}
        \caption{Mitose}
    \end{center}
\end{figure}

\vspace*{10pt}

Die einzelnen Phasen der Zellteilung:
\begin{itemize}
    \item \textbf{Prophase}: Kondensation (\glqq Aufwendeln\grqq) der Chromosomen.
        Diese werden dabei sichtbar.
    \item \textbf{Metaphase}: Die Chromosomen rücken in die Äquatorialebene.
        Die Kernmembran hat sich aufgelöst.
    \item \textbf{Anaphase}: Die (zwei identischen) Chromatiden eines jeden Chromosoms werden zufällig(!)
        zu den entgegengesetzten Zellpolen gezogen.
        (Durch \textit{Kinetochor}-/Chromosomenmikrotubuli.)
    \item \textbf{Telophase}: Chromatiden befinden sich in den Polen und werden dünner.
        In der Mitte der Zelle hat sich eine Zellplatte gebildet, die nach außen zuwächst.
\end{itemize}
$\rightarrow$ Eine neue Zellwand mit neuen Kernen hat sich gebildet und es
wurden somit zwei genetisch identische Zellen erzeugt.

\subsubsection{Meiose}
Bei der \textit{Meiose} (auch Reifeteilung, Reduktionsteilung) wird der Zellkern
\textbf{diploider Zellen} so geteilt, dass (im Unterschied zur Mitose) die Zahl der Chromosomen
halbiert wird, indem jede Tochterzelle jeweils einen homologen Partner eines Chromosomenpaares erhält.

Im Kernphasenwechsel\footnote{\textbf{Kernphase} ist der haploide, bzw. diploide Zustand der Zelle}
ist dies der Übergang von der diploiden zur haploiden Phase.\\

\textbox{Zusammenhang zwischen \textbf{Generationen- und Kernphasenwechsel}}{
    Im Pflanzenreich folgen oft verschiedene Generationen aufeinander.
    Bei einem solchen \textbf{Generationenwechsel} können sich die Gestalten
    und manchmal auch die Kernphase ändern.
    Dieser Entwicklungszyklus ist zwischen Haplonten, Diplonten und Haplo-Diplonten
    unterschiedlich.
}

\vspace*{10pt}

Damit einher geht gewöhnlich eine Rekombination, also eine neue
Zusammenstellung der elterlichen Chromosomen und zumeist auch von
Chromosomen-Abschnitten (Crossing-over).

\begin{enumerate}
    \item {Vor Meiose: \textit{Crossing over}. Väterliches und Mütterliches
        Chromosen legen sich aneinander und tauschen sich aus.}
    \item Bei Zellteilung, verteilen sich väterliche und mütterliche Chromosem
        zufällig an die Zellpole.
    \item Es entstehen vier haploide (halber Chromosomensatz) genetisch unterschiedliche
        Zellen
\end{enumerate}

Diplonten: Menschen, Tiere und Kieselalgen: Leben zeitlebens diploid, ausser
bei Keimzellen.\\
Haplonten: Leben zeitlebens haploid. Nur die befruchtete Eizelle ist zuerst diploid und
führt zunächst eine Meiose durch um wieder haploide Zellen zu liefern.\\
Generationswechsel: Bei fast allen Pflanzen und und Pilzen wechselt
sich eigenständige diploide Lebensform mit einer haploiden Lebensform
(Gametophyt) ab.

\begin{center}
    \underline{Mitschrift 11.11.2013}
\end{center}
\subsubsection{Generationswechsel}
Eine eigenständige diploide Lebensform (\textbf{Sporophyt}, z.B. (braune) Mooskapsel mit Stiel)
wechselt sich mit einer haploiden Lebensform (\textbf{Gametophyt}, z.B. grüne Moospflanze) ab.

\paragraph{Farne}
\label{sec:generationswechsel_farne}
\begin{figure}[H]
    \begin{center}
        \includegraphics[scale=0.5]{pics/botanik/alternation_of_generations_in_ferns.png}
        \caption{Generationswechsel der Farne}
    \end{center}
\end{figure}
Generationswechsel bei den Farnen:
\begin{enumerate}
    \item Der \textbf{Sporophyt} (diploid) ist die eigentliche Farnpflanze und die sporenbildende Generation.

    \item An der Blattunterseite seiner Blätter bilden sich \textbf{Sori} (singular \textbf{Sorus}), welche
        Ansammlungen von \textbf{Sporangien} (Behälter in welchen \underline{haploide} \textbf{Meiosporen}
        durch Meiose gebildet werden) sind.

    \item Nach der Reifung reißen die Sporangien auf, wobei die Sporen freigelassen werden und sich zu
        \textbf{Gametophyten} (Gametenbildende Generation) ausbilden.\\
        Dieser Gametophyt wird bei Farnen \textbf{Prothallium} genannt.

    \item Am reifen Gametophyt bilden sich männliche (\textbf{Antheridien})
        und weibliche Geschlechtsorgane (\textbf{Archegonien}) aus.\\
        Diese Geschlechtsorgane werden auch \textbf{Gametangien} - Behälter, in welchen
        Gameten (Spermatozoiden, Eizellen) gebildet werden - genannt.

    \item Durch Befruchtung der Eizelle durch die Spermatozoiden bildet sich eine \textbf{diploide Zygote}.

    \item Aus dieser Zygote entwickelt sich (\underline{auf dem Gametophyt}) wieder ein neuer Sporophyt.

    \item Nach weiterem Heranwachsen des Sporophyt vergeht der Gametophyt.

\end{enumerate}

\paragraph{Moose}
\label{sec:generationswechsel_moose}
\begin{figure}[H]
    \begin{center}
        \includegraphics[scale=0.7]{pics/botanik/generationswechsel_hornmoos.png}
        \caption{Generationswechsel Hornmoos}
    \end{center}
\end{figure}

\begin{itemize}
    \item Die eigentliche Moospflanze ist der \textbf{Gametophyt} (haploid).

    \item Männliche (\textbf{Antheridien}) und weibliche (\textbf{Archegonien})
        Geschlechtsorgane wachsen getrennt voneinander, wobei die Antheridien
        \textbf{begeißelte Spermatoziode} bilden, welche die
        \textbf{Eizellen} in den \textbf{Archegonien} über einen Wasserfilm hinweg befruchten.

    \item Aus der befruchtete Eizelle (\textbf{Zygote (2n)}) wächst der \textbf{diploide Sporophyt}
        (sporenbildende Generation).

    \item In dessen \textbf{Sporogon} (Sporenkapsel) werden \textbf{haploide Meiosporen} gebildet
        ($\rightarrow$ Sporogon ist als \textbf{Sporangium}) und frei gelassen.

    \item Aus diesem wächst wiederum der \textbf{Gametophyt}.
\end{itemize}

Der Generationswechsel der Moose ist folglich \textbf{heterophasisch} (Wechsel zwischen haploider und diploider
Generation) und \textbf{heteromorph} (Gametophyt und Sporophyt sehen unterschiedlich aus).\\

Beim \textbf{Brunnenlebermoos} gibt es eine spezielle \textbf{vegetative Vermehrung}.
Es werden Brutkörper in speziellen Brutbechern an der Unterseite des \textbf{\textit{Thallus}} (ganzer Körper) gebildet.
Durch Wassertropfen werden diese aus dem Becher geschlagen und keimen an einem neuen Ort.

\subsubsection{Fortpflanzung der Blütenpflanzen}
Besonderheit in Natur: Weibliche Pflanzen werden zweimal befruchtet.\\
Blüten sind Blätter die im Sinne der Fortpflanzung bestehen.
Bestandteile einer Standartblüte:
\begin{itemize}
    \item {Blüten(kelch)blätter: Oft grün und unscheinbar}
    \item {Kronblätter: Bunt gestaltet(Anlockung von Bestäubern)}
    \item {Staubblatt mit Staubbeutel (Sporophyll der über Meiose Pollen herstellt: Entsprechen
        männlichen Gametophyt) und Staubfaden.}
    \item {Fruchtblätter hüllen Samenanlagen ein. Zusammengewachsne bilden sie einen \textbf{Fruchtknoten}. Darin entwickelt sich der
        weibliche Gametophyt. $\rightarrow$ Fruchtknoten bildet Frucht}
\end{itemize}
Pollensack entspricht Mikrosporangium der heterosporen Farne. Pollen entsprechen den Mikrosporen.

Aus Staubblatt entwickelt sich haploider Poll.

\subsubsection{Frucht}
Die Frucht kann aus folgenden Teilen bestehen (Unterschiede zwischen Beere, Steinfrüchten, Nussfrucht):
\begin{itemize}
    \item Kern
    \item Endokarp (innerste Schicht)
    \item Mesokarp (Fruchtfleisch)
    \item Exokarp (Haut)
\end{itemize}

\subsubsection{Vegetative Fortpflanzung}
Pflanzen vermehren sich auch auf \glqq unorthodoxe\grqq Weise.

Zum Beispiel:
\begin{itemize}
    \item Brutzwiebeln
    \item Bryophyllum
    \item Walderdbeere: vegetative Verbreitung durch Stolone
\end{itemize}

\subsection{Photosynthese}
Primärproduktion von Sauerstoff weltweit am höchsten im \underline{offenen Meer}
(relativ geringe $O_2$-Produktion auf immerhin 65\% der Erdoberfläche).

Korallenriffe, Feuchtgebiete und Auen sowie immergrüner, tropischer Wald produzieren am meisten $O_2$.

\textit{Fun-Facts}:
\begin{itemize}
    \item Buche (ca. 1200 $m^2$ Gesamtfläche der Blätter und 180g Chlorophyll) - Umwandlung pro Tag:
        \begin{itemize}
            \item $9.3 m^3 CO_2 -> 9.3 m^3 O_2$ und
            \item $12kg$ Zucker (organisch) ($\rightarrow 10g$ Zucker pro $m^2$)
        \end{itemize}
    \item Photosynthese ist selbst noch an Orten möglich, an welchen nur $1\%$ des Sonnenlichtes ankommt.\\
        $\rightarrow$ in Meeren \textit{euphotischen Zone} genannt
\end{itemize}

\subsubsection{Ablauf der Photosynthese}
Photosynthese findet in den \textbf{Chloroplasten} statt.
Der Innenraum der Chloroplasten (\textbf{Stroma}) wird von
\textbf{Thylakoidmembranen} durchzogen.

In diesen Thyalakoiden findet die Lichtreaktion statt. (siehe ~\ref{fig:photo})\\

\textbox{\textbf{Brutto-Summenformel} der Photosynthese}{
    \begin{center}
    $6 CO_2 + 12H_2O \rightarrow C_6H_{12}O_6 + 6 H_2O + 6O_2$\footnote{
        Die Formel wird nicht gekürzt dargestellt, um zu zeigen, dass das in der
        Photosynthese erzeugte $O_2$ \underline{aus dem $H_2O$} und nicht aus dem $CO_2$ stammt.\\
        Beleg: Isotopenmarkierung des Wassers führt zu markiertem $O_2$.
        Analog führt markiertes $CO_2$ \underline{nicht} zu markiertem $O_2$.
    }
    \end{center}
}

Die Reaktion der Photosynthese ist zweigeteilt in Licht- (findet in den Thylakoiden statt) und Dunkelreaktion (findet in dem Stroma statt).

\begin{figure}[h]
    \begin{center}
        \includegraphics[scale=1.5]{pics/botanik/photosynthese_licht_dunkel.jpg}
        \caption{Photosynthese Licht- und Dunkelreaktion}
        \label{fig:photo}
    \end{center}
\end{figure}

\paragraph{Lichtreaktion}
Die Lichtreaktion verläuft in zwei Schritten:
\begin{enumerate}
    \item Die elektromagnetische Energie wird in Form von Licht geeigneter Wellenlänge unter
        Verwendung von Farbstoffen (Chlorophylle, Carotinoide, etc.) absorbiert.
    \item Umwandlung der gewonnenen elektromagnetischen Energie in chemische
        Energie durch Übertragung von Elektronen, die durch die Lichtenergie in einen
        energiereichen Zustand versetzt wurden (Redoxreaktion).
\end{enumerate}

Chlorophyll-a absobiert Licht im blauen und roten Spektrum (diese Wellenlängen führen
zu den höchsten Photosyntheseraten).
Andere Pigmente (z.B. Chlorophyll-b, Xanthophylle, etc.) können Licht aus
dem übrigen Wellenbereich absorbieren und dessen Energie auf Chlorophyll-a übertragen.\\

Nach der Lichtreaktion wurde ein Reduktionsmittel ($NADPH$) und eine Energiequelle ($ATP$)
für die folgende Dunkelreaktion gewonnen, wobei Sauerstoff als \underline{Nebenprodukt} entstand.

\paragraph{Dunkelreaktion}
Die Dunkelreaktion ist eine lichtunabhängige Reaktion und wird \textit{Calvin Zyklus} genannt.\\
Hierbei wird Kohlenstoffdioxid aus der Umgebungsluft zu  Kohlenhydraten (Zucker) reduziert.

\subsection{Fragenkatalog}
\begin{itemize}
    \item Unterschied Pro- und Eukaryonten
        \begin{itemize}
            \item Eukaryonten besitzen einen Zellkern (mit Doppelmembran), welcher die DNA
                (organisiert in Chromosomen) umgibt.
            \item Bei Prokaryonten befindet sich die DNA frei im Cytoplasma.
            \item Manche Bakterien besitzen zusätzliche DNA-Moleküle, sog. \textit{Plasmide}.
            \item Prokaryonten besitzen (kleinere) 70 S-Ribosomen
            \item Eukaryonten besitzen (größere) 80 S-Ribosomen
        \end{itemize}

    \item Unterschied zwischen tierischen und pflanzlichen Zellen
        \begin{itemize}
            \item Zellwand
            \item Vakuole
            \item Chloroplasten
        \end{itemize}

    \item verschiedene Genome in Pflanzenzelle
        \begin{itemize}
            \item Mitochondrien
            \item Chloroplasten
            \item Zellkern
        \end{itemize}
        $\rightarrow$ Endosymbiontentheorie:
        \label{Endosymbiontentheorie}
        Mitochondrien und Chloroplasten sind Endosymbionten, die sich aus
        eigenständigen Prokaryonten entwickelt haben. Dafür sprechen die 70 S-Ribosomen und eigene DNA.

    \item Wichtige Moleküle einer Pflanze (siehe ~\ref{sec:molekuele})
        \begin{itemize}
            \item Wasser
            \item DNA/RNA
            \item Fette und Fettsäuren
            \item Zucker
            \item Proteine
            \item Aminosäuren
        \end{itemize}

    \item Kormusbestandteile (Blatt, Wurzel, Spross) und deren Funktionen

    \item Anatomie
        \begin{itemize}
            \item Blattaufbau (Schichten im Querschnitt) und Metamorphosen (siehe ~\ref{sec:blatt})
            \item Metamorphosen und Aufgaben der Wurzeln
                (+ Unterschiede z. Spross) (siehe ~\ref{sec:wurzel})
        \end{itemize}

    \item Meiose und Mitose

    \item Generationswechsel von Farne/Moose

    \item Unterschied zwischen Sporen und Gameten

    \item {Photosynthese: Netto \& Bruttoformel\\
        Brutto Summenformel:\\
        $6 \text{CO}_2 + 12\text{H}_2\text{O} \rightarrow \text{C}_6\text{H}_{12}\text{O}_6 + 6\text{O}_2+6\text{H}_2\text{O}$\\
        Netto Summenformel:\\
        $6 \text{CO}_2 + 6\text{H}_2\text{O} \rightarrow \text{C}_6\text{H}_{12}\text{O}_6 + 6\text{O}_2$
    }
\item Unterschied zwischen Monokotyledonen und Dikotyledonen. (siehe ~\ref{unterschied_monokot_dikot})
\end{itemize}
