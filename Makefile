PDF = skript

all: $(PDF) 

continuous: $(PDF).continuous 

%.continuous: %.pdf
	latexmk -jobname=$(@:%.continuous=%) -pvc -pdf $(@:%.continuous=%).tex

$(PDF): $(PDF).pdf

%.pdf: %.tex
	latexmk -jobname=$(@:%.pdf=%) -pdf $<

clean:
	latexmk -C -f 

.PHONY: all clean $(PDF) continuous
